.. Copyright 2016 Joshua Taillon
..
.. Licensed under the Apache License, Version 2.0 (the "License");
.. you may not use this file except in compliance with the License.
.. You may obtain a copy of the License at
..
..     http://www.apache.org/licenses/LICENSE-2.0
..
.. Unless required by applicable law or agreed to in writing, software
.. distributed under the License is distributed on an "AS IS" BASIS,
.. WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
.. See the License for the specific language governing permissions and
.. limitations under the License.

FIBtortuosity
=============

**PyPI:** |pypiV| |pypiDM|

**Help!** |rtfd| |gitter|


..  |rtfd| image:: https://readthedocs.org/projects/fibtortuosity/badge/?version=latest
    :target: http://fibtortuosity.readthedocs.org/en/latest/?badge=latest
    :alt: Documentation Status

..  |gitter| image:: https://img.shields.io/gitter/room/nwjs/nw.js.svg
    :alt: Join the chat at https://gitter.im/jat255/FIBtortuosity
    :target: https://gitter.im/jat255/FIBtortuosity?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge

..  |pypiV| image:: https://img.shields.io/pypi/v/FIBtortuosity.svg
    :target: https://pypi.python.org/pypi/FIBtortuosity/

..  |pypiDM| image:: https://img.shields.io/pypi/dm/FIBtortuosity.svg
    :target: https://pypi.python.org/pypi/FIBtortuosity/

This package provides methods to calculate the geodesic distance and tortuosity of a reconstructed volume.
Author: Joshua Taillon (jat255@gmail.com)