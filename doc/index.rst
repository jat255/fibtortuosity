.. FIB Tortuosity documentation master file, created by
   sphinx-quickstart on Thu Mar 31 23:31:50 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

..  include::
    global.rst

FIB Tortuosity
==============

..  toctree::
    :caption: Table of Contents
    :maxdepth: 3

    intro.rst
    install.rst
    gen_instructions.rst
    api_doc.rst
