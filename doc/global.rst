..  csv-table::
    :escape: \
    :widths: 10, 15, 20

    **PyPI:**, |pypiV|, |pypiDM|
    **Help!**, |rtfd|, |gitter|


..  |rtfd| image:: https://readthedocs.org/projects/fibtortuosity/badge/?version=latest
    :target: http://fibtortuosity.readthedocs.org/en/latest/?badge=latest
    :alt: Documentation Status

..  |gitter| image:: https://img.shields.io/gitter/room/nwjs/nw.js.svg
    :alt: Join the chat at https://gitter.im/jat255/FIBtortuosity
    :target: https://gitter.im/jat255/FIBtortuosity?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge

..  |pypiV| image:: https://img.shields.io/pypi/v/FIBtortuosity.svg
    :target: https://pypi.python.org/pypi/FIBtortuosity/

..  |pypiDM| image:: https://img.shields.io/pypi/dm/FIBtortuosity.svg
    :target: https://pypi.python.org/pypi/FIBtortuosity/