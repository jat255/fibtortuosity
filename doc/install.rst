..  include::
    global.rst

Installation
============

Requirements
++++++++++++

The following dependencies are needed to run the code (as-written). Some of
these could be pretty easily removed, but the code was written with my research
results in mind, and as such, has some dependencies that make things match
my personal preferences (such as ``seaborn``). Some details on installing them
are given below:

..  csv-table::
    :header: Package, Source, PyPI
    :escape: \

    ``numpy``, `Source <Numpy_>`_, `PyPI <NumpyPYPI_>`_
    ``matplotlib``, `Source <matplotlib_>`_, `PyPI <matplotlibPYPI_>`_
    ``libtiff``, `Source <libtifflib_>`_ , N/A
    ``pylibtiff``, `Source <pylibtiffsource_>`_\, `Python 3 port <jatPylibtiff_>`_, `PyPI <pylibtiffPYPI_>`_
    ``scikit-fmm``, `Source <scikitfmm_>`_, `PyPI <scikitfmmPYPI_>`_
    ``seaborn``, `Source <seaborn_>`_, `PyPI <seabornPYPI_>`_
    ``hyperspy`` `(optional)`, `Source <HyperSpy_>`_, `PyPI <HyperSpyPYPI_>`_

.. _Numpy: http://www.numpy.org/
.. _NumpyPYPI: https://pypi.python.org/pypi/numpy/1.11.0
.. _matplotlib: http://matplotlib.org/
.. _matplotlibPYPI: https://pypi.python.org/pypi/matplotlib/1.5.1
.. _libtifflib: http://www.remotesensing.org/libtiff/
.. _pylibtiffsource: https://github.com/pearu/pylibtiff
.. _pylibtiffPYPI: https://pypi.python.org/pypi/libtiff/
.. _jatPylibtiff: https://github.com/jat255/pylibtiff/tree/ENH_py3_upgrade
.. _scikitfmm: https://github.com/scikit-fmm/scikit-fmm
.. _scikitfmmPYPI: https://pypi.python.org/pypi/scikit-fmm
.. _seaborn: https://stanford.edu/~mwaskom/software/seaborn/
.. _seabornPYPI: https://pypi.python.org/pypi/seaborn
.. _pytz: http://pytz.sourceforge.net/
.. _pytzPYPI: https://pypi.python.org/pypi/pytz/
.. _HyperSpy: http://www.hyperspy.org/
.. _HyperSpyPYPI: https://pypi.python.org/pypi/hyperspy/0.8.4


Python 2.7
++++++++++

To get a full set-up on Python 2.7 (hopefully) pretty easily, run the following:

.. code-block:: bash

    $ pip install numpy matplotlib libtiff scikit-fmm seaborn fibtortuosity hyperspy

Python 3.5
++++++++++

On Python 3.5, things are a little bit trickier because one of the
dependencies (|libtiff|_) has not yet been ported to Python 3.
I have done my best to port this package myself (currently awaiting
inclusion into the main |libtiff|_ package). I cannot guarantee it
will work, but it seems alright on my machine. To install my fork, open up a
command line and run the following:

.. code-block:: bash

    $ git clone https://github.com/jat255/pylibtiff.git
    $ cd pylibtiff
    $ git checkout ENH_py3_upgrade
    $ pip install .

This library depends on a compiled version of the |tiff|_ library as well.
On a linux system (like Ubuntu) this library is probably available in the
package repository, and can be installed with the following:

.. code-block:: bash

    $ sudo apt-get install libtiff5 libtiff5-dev

On Windows, things are just a little bit harder. I was able to get it
built on my Windows box by follow the instructions
`here <http://www.remotesensing.org/libtiff/build.html#PC>`_.

Some tips:
    * Make sure to compile the version (32 or 64 bit) that matches your Python installation
    * `This <https://msdn.microsoft.com/en-us/library/x4d2c09s.aspx>`_ page will explain how to get the Visual C++ compiler to work on the command line
    * Once you have compiled the ``libtiff.dll`` file, add the directory containing it to your system path, and then restart any Python environments you have open. The library should now be available...

Once all that is done, run the same code as for 2.7, but take out `libtiff` from the ``pip`` list.


"Stable" Version
++++++++++++++++

Although it shouldn't really be considered stable, there are released versions
available for installation through ``pip`` on
`PyPI <https://pypi.python.org/pypi/FIBTortuosity>`_.

.. note::   Note: due to some recent bugs with PyPI, the package will likely
            not show up if you use ``pip search``, but should install as
            normal with ``pip install``.

To install the latest release:

.. code-block:: bash

    $ pip install --user fibtortuosity


Development Version
+++++++++++++++++++

The latest version of the code should be available in the Bitbucket
`repository <https://bitbucket.org/jat255/fibtortuosity.git>`_.
To get this version installed on your system, clone the repository,
and then install with ``pip``:

.. code-block:: bash

    $ git clone https://bitbucket.org/jat255/fibtortuosity.git
    $ cd fibtortuosity
    $ pip install -e ./

.. |libtiff| replace:: ``libtiff``
.. _libtiff: https://pypi.python.org/pypi/libtiff/

.. |tiff| replace:: ``tiff``
.. _tiff: http://www.remotesensing.org/libtiff/build.html