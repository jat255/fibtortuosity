..  include::
    global.rst

Introduction
============
This module is used to calculate the geometric tortuosity of a
three-dimensional structure, based on J. Taillon's 2018 *Ultramicroscopy* paper_. 
Useful for FIB/SEM reconstructions of SOFCs, but probably useful
in other situations as well.

These methods have been written for the specific software and methods used
in our lab, and as such, might need some tweaking to work with your data. In
general, .tiff files used as inputs are expected in the format that is
output by Avizo_. There is part of the code that depends on the
``ImageDescription`` tiff tag that Avizo saves. This tag saves information
about the physical dimensions of the bounding box, and is of the
format: ``'BoundingBox <minX> <maxX> <minY> <maxY> <minZ> <maxZ>\n'``
For the sample files in this documentation, the value is:
``'BoundingBox 3231.15 6462.29 0 4100.38 0 4000\n'``
If you cannot write this information into the tiff file that you are trying
to provide, the code will have to be modified.

The expected inputs are binary (0 and 1) or ternary (0, 1, or 2)
labeled 3D tif files representing the phases to calculate. Because the
module is focused on SOFC calculations primarily, it is expected that 1 index
represents the bulk electrolyte phase. If present, the 2 index is the phase
for which to calculate the tortuosity. The 0 index are the areas that cannot
be traversed (i.e. the hard boundaries that create the tortuosity).

For convenience, two small test 3D tiff files are provided for the example
calculated in the :doc:`/gen_instructions` section. Those can be downloaded
here: :download:`Electrolyte <example_electrolyte.tif>` and
:download:`Cathode <example_electrolyte_and_cathode.tif>`.

.. _paper: https://doi.org/10.1016/j.ultramic.2017.07.017
.. _Avizo: http://www.fei.com/software/avizo3d/
